Source: libtext-undiacritic-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Mason James <mtj@kohaaloha.com>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-tiny-perl,
               perl
Build-Depends-Indep: libtest-simple-perl <!nocheck>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libtext-undiacritic-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libtext-undiacritic-perl.git
Homepage: https://metacpan.org/release/Text-Undiacritic
Rules-Requires-Root: no

Package: libtext-undiacritic-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Description: module to remove diacritics from a string
 Text::Undiacritic provides a module that changes characters with diacritics
 into their base characters.
 .
 Also changes into base character in cases where UNICODE does not provide a
 decomposition.
 .
 E.g. all characters '... WITH STROKE' like 'LATIN SMALL LETTER L WITH STROKE'
 do not have a decomposition. In the latter case the result will be 'LATIN
 SMALL LETTER L'.
 .
 Removing diacritics is useful for matching text independent of spelling
 variants.
